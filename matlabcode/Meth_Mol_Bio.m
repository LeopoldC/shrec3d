
clear all;
close all;

%resolution in bp
R=100000;

%Import Data
A= load('/data/HiC/human/GM12878_combined/1kb_resolution_intrachromosomal/chr1/MAPQGE30/chr1_1kb.RAWobserved');
B=importdata('/home/carron/Documents/Code/codenatbiomol/E116_15_coreMarks_dense');
color=B.data(ismember(B.textdata,'chr1'),:);


%%%%%%%%%%%%%%% Build matrix
list= cat(1,[A(:,1),A(:,2),A(:,3)],[A(:,2),A(:,1),A(:,3)]);
test=sparse(list(:,1)+1,list(:,2)+1,list(:,3)); %Build matrix at bp resolution in sparse format
Data=bin2d(test,R,R); %change to desired resolution (see bin2d fucntion)

%%%%%%%%%%%%%%% Build color annotation at desired resolution

% create vector of bin boundaries
G=1:R:length(test)*R;

%color the beads
number=max(color(:,3));
color_vec=zeros(length(test),number);
for i=1:length(color)
    color_vec(color(i,1)+1:color(i,2)+1,color(i,3))=1;
end
color_bins=bin2d(color_vec,R,1);
color_bins=color_bins./max(color_bins);

%%%%%%%%%%%%%%%    Filtering
Data=Data-diag(diag(Data));
figure, hist(sum(Data),100);
mini= mean(sum(Data))-1.5*std(sum(Data)) % Min number of contact is mean - 1.5 std
maxi= mean(sum(Data))+1.5*std(sum(Data)) % Max number of contact is mean + 1.5 std

Data2=Data(sum(Data)<maxi & sum(Data)>mini,sum(Data)<maxi & sum(Data)>mini);
color2=color_bins(sum(Data)<maxi & sum(Data)>mini,:);

%%%%%%%%%%%%%% Normalize the filtered matrix using SCN (Cournac et al.,2012)
Data2=SCN_sumV2(Data2); % See SCN fucntion
figure, imagesc(log10(Data2));


%%%%%%%%%%%%%% 3D STRUCTURE
alpha=0.227

%Compute distance matrix
distfromcont=FastFloyd(1./(Data2.^alpha)); % Compute distance matrix (see FastFloyd.m fucntion)
distfromcont=distfromcont-diag(diag(distfromcont));

%Multi-dimensional scaling
crit='sammon'; % Choose between 'sammon' and 'strain'
XYZ=mdscale(distfromcont,3,'Criterion',crit);

% Rescale coordinates to that the Volume is 100
[pts, V]= convhull(XYZ);
scale=100/V^(1/3);
XYZ=XYZ*scale;

% Create a PDB file with the structure
data.X = XYZ(:,1);
data.Y = XYZ(:,2);
data.Z = XYZ(:,3);
data.atomName = cell(1,length(XYZ));
data.atomName(1:end) = {'CA'}; %Label atoms as CA to use the tube represention in VMD
colorID=1;
data.betaFactor=full(color2(:,colorID)); %Label Atoms with colors using the Beta Factor field
str=sprintf('_%1.2f.pdb',alpha)
filename=strcat('3Dcolors_',crit,str)
data.outfile = filename;
mat2pdb(data) % See mat2pdb.m function
